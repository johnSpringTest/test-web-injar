package com.john.test.web.injar.score.controller;

import com.john.test.web.injar.score.data.ScoreVo;
import com.john.test.web.injar.score.service.IScoreService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;

/**
 * @author jiangguangtao on 2016/4/19.
 */
@Controller
@RequestMapping("/score")
public class ScoreController {

    private IScoreService scoreService;

    @Resource
    public void setScoreService(IScoreService scoreService) {
        this.scoreService = scoreService;
    }

    /**
     * 跳转到成绩页面
     *
     * @return
     */
    @RequestMapping("/toScoreList")
    public String toScoreList(Model model) {
        model.addAttribute("scoreList", scoreService.queryAll());
        return "score/score_list";
    }

    /**
     * 跳转到新增成绩页面
     *
     * @param model
     * @return
     */
    @RequestMapping("/toAddScore")
    public String toAddScore(Model model) {

        model.addAttribute("score", new ScoreVo());
        return "score/score_edit";
    }

    /**
     * 跳转到修改页面
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping("/toEditScore")
    public String toEditScore(Model model, Long id) {
        ScoreVo scoreVo = scoreService.findById(id);
        model.addAttribute("score", scoreVo);
        return "score/score_edit";
    }

    /**
     * 新增成绩
     *
     * @param model
     * @param scoreVo
     * @return
     */
    @RequestMapping(value = "/addScore", method = RequestMethod.POST)
    public String addScore(Model model, ScoreVo scoreVo) {
        scoreService.saveOrUpdate(scoreVo);
        return "redirect:toScoreList";
    }

    /**
     * 删除指定的成绩单
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping("/removeScore")
    public String removeScore(Model model, Long id) {
        scoreService.deleteById(id);
        return "redirect:toScoreList";
    }
}
