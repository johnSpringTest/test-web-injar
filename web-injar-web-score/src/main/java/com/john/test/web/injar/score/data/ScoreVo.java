package com.john.test.web.injar.score.data;

import com.john.test.web.injar.core.data.BaseVo;

/**
 * Created by jiangguangtao on 2016/12/26.
 */
public class ScoreVo extends BaseVo {
    private String userName;
    private String courseName; //课程名称
    private Float baseScore;
    private Float score;
    private String remark;

    public ScoreVo() {
    }

    public ScoreVo(String userName, String courseName, Float baseScore, Float score) {
        this.userName = userName;
        this.courseName = courseName;
        this.baseScore = baseScore;
        this.score = score;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Float getBaseScore() {
        return baseScore;
    }

    public void setBaseScore(Float baseScore) {
        this.baseScore = baseScore;
    }

    public Float getScore() {
        return score;
    }

    public void setScore(Float score) {
        this.score = score;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "ScoreVo{" +
                ", id=" + getId() +
                "userName='" + userName + '\'' +
                ", courseName='" + courseName + '\'' +
                ", baseScore=" + baseScore +
                ", score=" + score +
                ", remark='" + remark + '\'' +
                '}';
    }
}
