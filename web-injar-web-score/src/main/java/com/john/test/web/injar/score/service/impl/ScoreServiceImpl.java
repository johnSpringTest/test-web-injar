package com.john.test.web.injar.score.service.impl;

import com.john.test.web.injar.score.data.ScoreVo;
import com.john.test.web.injar.score.service.IScoreService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by jiangguangtao on 2016/12/27.
 */
@Service
public class ScoreServiceImpl implements IScoreService, InitializingBean {
    private static AtomicLong SCORE_ID = new AtomicLong(0);
    private static Map<Long, ScoreVo> scoreVoMap = new HashMap<>();


    @Override
    public void afterPropertiesSet() throws Exception {
        saveOrUpdate(new ScoreVo("John", "CN", 60.0f, 67.0f));
        saveOrUpdate(new ScoreVo("John", "EN", 80.0f, 62.0f));
        saveOrUpdate(new ScoreVo("Peter", "CN", 60.0f, 82.0f));
        saveOrUpdate(new ScoreVo("Peter", "EN", 80.0f, 90.0f));
    }

    @Override
    public List<ScoreVo> queryAll() {
        List<ScoreVo> list = new ArrayList<ScoreVo>(scoreVoMap.size());
        list.addAll(scoreVoMap.values());
        return list;
    }

    @Override
    public Long saveOrUpdate(ScoreVo scoreVo) {
        if (null == scoreVo.getId()) {
            scoreVo.setId(SCORE_ID.incrementAndGet());
        }
        scoreVoMap.put(scoreVo.getId(), scoreVo);
        return scoreVo.getId();
    }

    @Override
    public ScoreVo findById(Long id) {
        return scoreVoMap.get(id);
    }

    @Override
    public void deleteById(Long id) {
        scoreVoMap.remove(id);
    }

}
