package com.john.test.web.injar.score.service;

import com.john.test.web.injar.score.data.ScoreVo;

import java.util.List;

/**
 * Created by jiangguangtao on 2016/12/27.
 */
public interface IScoreService {

    List<ScoreVo> queryAll();

    Long saveOrUpdate(ScoreVo scoreVo);

    ScoreVo findById(Long id);

    void deleteById(Long id);
}
