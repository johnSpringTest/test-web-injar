package com.john.test.web.injar.core.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 〈一句话功能简述)
 * 〈功能详细描述〉
 *
 * @author jiangguangtao on 2016/4/19.
 */
@Controller
public class CoreHomeController {

    @RequestMapping(value = {"/", "/home"})
    public String home(Model model) {
        model.addAttribute("recipient", "Peter");
        return "home";
    }
}
