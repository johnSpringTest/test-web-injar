package com.john.test.web.injar.core.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.ContextStoppedEvent;
import org.springframework.stereotype.Service;

/**
 * 只能正常监听到刷新、关闭事件
 * Created by jiangguangtao on 2016/12/27.
 */
@Service
public class AppMonitor implements ApplicationListener<ApplicationContextEvent> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AppMonitor.class);

    @Override
    public void onApplicationEvent(ApplicationContextEvent event) {
//如果容器关闭时，触发
        if (event instanceof ContextClosedEvent) {
            LOGGER.info("#####################");
            LOGGER.info("容器关闭");
            LOGGER.info("#####################");
        }
        //容器刷新时候触发
        if (event instanceof ContextRefreshedEvent) {
//            ContextRefreshedEvent cre = (ContextRefreshedEvent) event;
            LOGGER.info("#####################");
            LOGGER.info("容器刷新");
//            LOGGER.info(cre);
            LOGGER.info("#####################");
        }
        //容器启动的时候触发
        if (event instanceof ContextStartedEvent) {
//            ContextStartedEvent cse = (ContextStartedEvent) event;
            LOGGER.info("#####################");
            LOGGER.info("容器启动");
//            LOGGER.info(cse);
            LOGGER.info("#####################");
        }
        //容器停止时候触发
        if (event instanceof ContextStoppedEvent) {
//            ContextStoppedEvent cse = (ContextStoppedEvent) event;
            LOGGER.info("#####################");
            LOGGER.info("容器停止");
//            LOGGER.info(cse);
            LOGGER.info("#####################");
        }
    }
}
