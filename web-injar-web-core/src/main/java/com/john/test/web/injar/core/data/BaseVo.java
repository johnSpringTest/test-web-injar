package com.john.test.web.injar.core.data;

import java.io.Serializable;

/**
 * 基础的Vo对象
 * Created by jiangguangtao on 2016/12/26.
 */
public class BaseVo implements Serializable {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
