package com.john.test.web.injar.user.data;

import com.john.test.web.injar.core.data.BaseVo;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author jiangguangtao on 2016/4/19.
 */
public class UserInfo extends BaseVo {
    private static final long serialVersionUID = -8095634382769651206L;
    private String name;
    private String address;
    private Date addTime;

    public UserInfo() {
        this.addTime = new Date();
    }

    public UserInfo(String name, String address) {
        this();
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", addTime=" + addTime +
                '}';
    }
}
