package com.john.test.web.injar.user.service;

import com.john.test.web.injar.user.data.UserInfo;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 〈一句话功能简述〉<br/>
 * 〈功能详细描述〉
 *
 * @author jiangguangtao on 2016/4/19.
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@Service
public class UserInfoServiceImpl implements IUserInfoService, InitializingBean {
    private static AtomicLong USER_ID = new AtomicLong(0L);
    private Map<Long, UserInfo> userMap = new HashMap<Long, UserInfo>();


    public void afterPropertiesSet() throws Exception {
        saveOrUpdate(new UserInfo("John", "河南沈丘"));
        saveOrUpdate(new UserInfo("John", "上海的一个地方"));
        saveOrUpdate(new UserInfo("Mater", "Hangz"));
    }

    public List<UserInfo> queryUserList() {
        List<UserInfo> list = new ArrayList<UserInfo>(userMap.size());
        list.addAll(userMap.values());
        Collections.sort(list, new Comparator<UserInfo>() {
            public int compare(UserInfo o1, UserInfo o2) {
                return o1.getId().compareTo(o2.getId());
            }
        });
        return list;
    }

    public Long saveOrUpdate(UserInfo userInfo) {
        if(null == userInfo.getId()) {
            userInfo.setId(USER_ID.incrementAndGet());
        }
        userInfo.setAddTime(new Date());
        userMap.put(userInfo.getId(), userInfo);
        return userInfo.getId();
    }

    @Override
    public UserInfo findById(Long id) {
        return userMap.get(id);
    }

    @Override
    public void deleteById(Long id) {
        userMap.remove(id);
    }
}
