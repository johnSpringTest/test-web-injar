package com.john.test.web.injar.user.controller;

import com.john.test.web.injar.user.data.UserInfo;
import com.john.test.web.injar.user.service.IUserInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;

/**
 * 〈一句话功能简述〉<br/>
 * 〈功能详细描述〉
 *
 * @author jiangguangtao on 2016/4/19.
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@Controller
@RequestMapping("/user")
public class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    private IUserInfoService userInfoService;

    @Resource
    public void setUserInfoService(IUserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    /**
     * 跳转到用户列表
     *
     * @param model
     * @return
     */
    @RequestMapping("/list")
    public String list(Model model) {
        model.addAttribute("userList", userInfoService.queryUserList());
        return "user/user_list";
    }

    /**
     * 添加到新增用户界面
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String toAdd(Model model) {
        model.addAttribute("user", new UserInfo());
        return "user/user_edit";
    }

    /**
     * 跳转到修改用户信息页面
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String toEdit(Model model, Long id) {
        UserInfo user = userInfoService.findById(id);
        model.addAttribute("user", user);
        return "user/user_edit";
    }

    /**
     * 保存用户
     * @param model
     * @param userInfo
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(Model model, UserInfo userInfo) {
        userInfoService.saveOrUpdate(userInfo);
        return list(model);
    }

    /**
     * 删除用户信息
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/remove")
    public String remove(Model model, Long id) {
        userInfoService.deleteById(id);
        return list(model);
    }
}
