package com.john.test.web.injar.user.service;

import com.john.test.web.injar.user.data.UserInfo;

import java.util.List;

/**
 * 用户服务类
 *
 * @author jiangguangtao on 2016/4/19.
 */
public interface IUserInfoService {
    /**
     * 查询所有用户列表
     *
     * @return
     */
    List<UserInfo> queryUserList();

    /**
     * 添加或者更新用户
     *
     * @param userInfo
     * @return
     */
    Long saveOrUpdate(UserInfo userInfo);

    UserInfo findById(Long id);

    void deleteById(Long id);
}
